<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Student;

use Image;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return view('student.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'student_id' => 'required|numeric',
            'student_img' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($request->hasFile('student_img')) {
            $file = $request->file('student_img');
            $extension = $file->extension();
            $final = date('YmdHis').'.'.$extension;

            Image::make($file)->resize(200, 200)->save(public_path('/uploads/').$final);
        };

        $student = new Student();
        $student->name = $request->name;
        $student->student_id = $request->student_id;
        $student->student_img = $final;
        $student->save();

        return redirect()->route('student.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::findOrFail($id);
        return view('student.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::findOrFail($id);

        if ($request->hasFile('student_img')) {
            if (file_exists(public_path('/uploads/'.$student->student_img))) {
                unlink(public_path('/uploads/'.$student->student_img));
            }

            $request->validate([
                'student_img' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $file = $request->file('student_img');
            $extension = $file->extension();
            $final = date('YmdHis').'.'.$extension;

            // Image::make($file)->resize(100, 100)->insert(name/name.png, 'bottom')->save(public_path('/uploads/').$final);
            Image::make($file)->resize(100, 100)->save(public_path('/uploads/').$final);

            $student->student_img = $final;
        }

        $request->validate([
            'name' => 'required',
            'student_id' => 'required|numeric',
        ]);

        $student->name = $request->name;
        $student->student_id = $request->student_id;
        $student->update();

        return redirect()->route('student.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrFail($id);

        if (file_exists(public_path('/uploads/'.$student->student_img))) {
            unlink(public_path('/uploads/'.$student->student_img));
        }
        $student->delete();

        return redirect()->route('student.index');
    }
}
