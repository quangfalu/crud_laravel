@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="mt-5">
                <form action="{{ route('student.update', $student->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="productName" class="form-label">Student Name</label>
                        <input type="text" value="{{ $student->name }}" class="form-control" id="name" name="name"
                            placeholder="Student Name">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="student-id" class="form-label">Product Price</label>
                        <input type="text" value="{{ $student->student_id }}" class="form-control" id="student_id"
                            name="student_id" placeholder="Student ID">
                        @error('student_id')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Student Image</label>
                        <input type="file" class="form-control" id="student_img" name="student_img"
                            placeholder="Student Image">
                        @error('student_img')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <img src="{{ url('/uploads', $student->student_img) }}" alt="">
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
